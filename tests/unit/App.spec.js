import { mount } from '@vue/test-utils'
import App from '@/App.vue'
import AppBar from '@/components/AppBar.vue'

describe('App.vue', () => {
  it('renders AppBar component', () => {
    const wrapper = mount(App)
    expect(wrapper.findComponent(AppBar).exists()).toBe(true)
  })

  it('renders router-view', () => {
    const wrapper = mount(App)
    expect(wrapper.findComponent({ name: 'router-view' }).exists()).toBe(true)
  })

  // Add more tests as needed
})
